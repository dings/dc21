# -*- coding: utf-8 -*-
from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.core.management.base import BaseCommand
from django.template import engines

from debconf.tz import aoe_datetime
from register.models import QueueSlot


SUBJECT = "DebConf21 Registration: T-Shirt Details"

TEMPLATE = """\
Dear {{ name }},

We're getting ready to print and ship DebConf21 T-Shirts.

You selected a {{ t_shirt_size }} t-shirt, to be shipped to:
{{ shipping_address }}

{% if fitted_cut %}
You selected a fitted cut t-shirt. These shirts are cut tightly, approximately
3 sizes smaller than the corresponding size straight cut.{% else %}You selected a straight cut t-shirt.{% endif %}

If any of these details are incorrect, please update your registration on the
DebConf21 website <https://debconf21.debconf.org/register/>, and then reply to
this email, to let the registration team know of the change.

You registered before the t-shirt deadline; however, we can't guarantee that
everyone who did so will receive a t-shirt.  We only have a limited budget and
we may not be able to deliver to everyone or every country in time, but we'll
do our best.

Depending on where you live, you may also have to pay customs duties.  We are
planning to print and ship t-shirts from the US, EU, Brazil, India, South
Africa, and Taiwan.  If you live outside these areas, you should expect to have
to pay customs duties.  If you are not willing to pay them, please cancel your
t-shirt request.

Looking forward to seeing you online,

The DebConf21 T-shirt team
"""

class Command(BaseCommand):
    help = 'Send tshirt confirmation emails'

    def add_arguments(self, parser):
        parser.add_argument('--yes', action='store_true',
                            help='Actually do something'),
        parser.add_argument('--username',
                            help='Send mail to a specific user, only'),

    def badger(self, attendee, dry_run):
        name = attendee.user.userprofile.display_name()
        to = attendee.user.email
        size = attendee.t_shirt_size

        ctx = {
            'fitted_cut': size.endswith('_f'),
            'name': name,
            'shipping_address': attendee.shipping_address.formatted_address,
            't_shirt_size': dict(settings.DEBCONF_T_SHIRT_SIZES)[size],
        }

        template = engines['django'].from_string(TEMPLATE)
        body = template.render(ctx)

        if dry_run:
            print('Would badger %s <%s>' % (name, to))
            return

        email_message = EmailMultiAlternatives(
            SUBJECT, body, to=["%s <%s>" % (name, to)])
        email_message.send()

    def handle(self, *args, **options):
        dry_run = not options['yes']
        if dry_run:
            print('Not actually doing anything without --yes')

        queryset = QueueSlot.objects.filter(
            queue__name='Swag',
            timestamp__lt=aoe_datetime(settings.DEBCONF_CONFIRMATION_DEADLINE),
            attendee__completed_register_steps=8,
        ).exclude(
            attendee__t_shirt_size='',
        )
        if options['username']:
            queryset = queryset.filter(
                attendee__user__username=options['username'])

        for queue_slot in queryset:
            self.badger(queue_slot.attendee, dry_run)
