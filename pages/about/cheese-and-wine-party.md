---
name: Cheese and Wine Party
---
# Cheese and Wine Party

Cheese is good for you. Wine also helps.
This will be an unofficial online DebConf Cheese and Wine party.
The 0th party was improvised in Helsinki during DebConf 5, in the
so-called "French" room.
It then found its way into the official timetable at DebConf 6, in
Mexico, and has been a DebConf tradition ever since.

The concept is usually very simple: bring tasty things from your
country, preferably edible.
Grapes and lactose are both optional: we like cheese and wine, but we
love the surprising things that people bring from all around the world.
Even if you do not bring anything, feel free to participate: our
priorities are our users and free cheese.

The online format changes things a little.
Here, we're inviting short videos of up to 30 seconds' length, showing
and introducing an item you might have brought to an in-person Cheese
and Wine event.

Some bits from previous editions:

 * [C&W13, DebConf17, Montreal, Canada, August 7th 2017](https://www.flickr.com/photos/aigarius/36398984276/in/photostream/)
 * [C&W14, DebConf18, Hsinchi, Taiwan, August 30th  2018](https://photos.google.com/share/AF1QipOroi0hV38ezF2Oa_5CmtpIvdam2rLJ2Dy6oIyzGD6AJ_Emfrrh573YxmyVoS-vUQ/photo/AF1QipNQYvb8ViztpXfkg3neI4eV0HsMC5Yt7GSayFA7?key=UEZ5VVphTlJhS2xTTl9vVjdxM0ZmbWlFcC1rTHBB)
 * [DebConf19, Curitiba, Brazil, July 22nd  2019](https://photos.google.com/share/AF1QipPekZe8o40CK9YpAu3zXcmFQW28WNMfk_x9R86I9vdFQjhwsIpyatG9f7WN2GXvCw/photo/AF1QipMYMtiBzRDPsSxmVaUXgahD-MBrYGN_Jyfu7g3C?key=b0lJUXVmLUJ2MjJQYTJ4c1NZdmtQT0hxT2JSZlBR)
 * [DebConf20, Online](https://debconf20.debconf.org/talks/69-cheese-and-wine-at-home/)

## When?

FIXME

## Where?

Online!
