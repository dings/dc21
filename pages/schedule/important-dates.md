---
name: Important Dates
---
<style type="text/css">
tr td:first-child {
  width: 10rem;
}
</style>


| **APRIL**             |                                                                      |
|-----------------------|----------------------------------------------------------------------|
| 3 (Saturday)          | Decision on having the physical conference                           |

| **MAY**               |                                                                      |
|-----------------------|----------------------------------------------------------------------|
| 22 (Saturday)         | Opening of the [Call For Proposals](/cfp/)                           |

| **JUNE**              |                                                                      |
|-----------------------|----------------------------------------------------------------------|
| 5 (Saturday)          | Opening attendee registration                                        |
| 20 (Sunday)           | Last day for submitting a talk that will be considered for the official schedule. |

| **JULY**              |                                                                      |
|-----------------------|----------------------------------------------------------------------|
| 4 (Sunday)            | Last date to apply for an expenses bursary. |
| 25 (Sunday)           | Last day to register with guaranteed swag. Registrations after this date are still possible, but swag is not guaranteed. |

| **AUGUST**            |                                                                      |
|-----------------------|----------------------------------------------------------------------|
| 7 (Saturday)          | Recommended date for presenters to submit pre-recorded talks, so that they can get feedback from the content team. |
| 14 (Saturday)         | Last date for presenters to submit pre-recorded talks to the video team. |
| *DebCamp*             |                                                                      |
| 15 (Sunday)           | First day of DebCamp                                                 |
| 16 (Monday)           | Second day of DebCamp                                                |
| 17 (Tuesday)          | Third day of DebCamp                                                 |
| 18 (Wednesday)        | Fourth day of DebCamp                                                |
| 19 (Thursday)         | Fifth day of DebCamp                                                 |
| 20 (Friday)           | Sixth day of DebCamp                                                 |
| 21 (Saturday)         | Seventh day of DebCamp                                               |
| 22 (Sunday)           | Eighth day of DebCamp                                                |
| 23 (Monday)           | Ninth day of DebCamp                                                 |
| *DebConf*             |                                                                      |
| 24 (Tuesday)          | First day of DebConf                                                 |
| 25 (Wednesday)        | Second day of DebConf                                                |
| 26 (Thursday)         | Third day of DebConf                                                 |
| 27 (Friday)           | Fourth day of DebConf                                                |
| 28 (Saturday)         | Last day of DebConf / closing ceremony / teardown                    |
| 29 (Sunday)           | Regular life resumes                                                 |
